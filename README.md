# scrfl

This is a
[ScreenFloat](https://eternalstorms.at/ScreenFloat/ScreenFloat_-_Make_your_Shots_fly/ScreenFloat_-_Make_your_Shots_fly%21.html)-like
tool which floats screenshots on your desktop for easy reference. It aids
workflow on laptops and minimises jumping to different workspaces when only a
single screen is available. It always opens and stays on top of all workspaces.

## Set up

I use it in a GNOME environment in conjunction with the [ScreenShot tool](https://extensions.gnome.org/extension/1112/screenshot-tool/) GNOME Extension.

To install:
 * clone the repo
 * make sure you have Python 3.6 or better installed
 * make sure you have `pipenv` installed
 * run `pipenv install` to install the dependencies

### Current (hacky) setup

As the aforementioned ScreenShot tool doesn't yet accept a viewer to spawn on a
screen grab, I have a rather ugly hack to invoke this script on every
screenshot.

I run the following script on startup (set up with `gnome-session-properties`) which 
basically monitors for screen grabs and then invokes `scrfl.py` whenever one is taken.

```bash
#!/usr/bin/env bash
set -x

SCRFL_DIR=/home/brad/Code/scrfl

dbus-monitor "interface='org.gnome.Shell.Screenshot'" | \
        grep --line-buffered "string \"/tmp" | \
        while read line
        do
                filename=$(echo $line | sed -E 's/^.+\"(.+)\"/\1/')
                sleep 0.3
    /home/brad/.pyenv/shims/python $SCRFL_DIR/scrfl.py $filename &
        done
```

### Future (elegant) setup

When the maintainers of GNOME's screenshot tool provide the ability to invoke a specific executable on screenshot (see [this Github issue](https://github.com/OttoAllmendinger/gnome-shell-screenshot/issues/15)) then we should be able to ditch the script above

## Usage

This thing is super minimal. Here are all the things you can do:
 * resize by grabbing any corner and dragging
 * move by dragging from (roughly) the middle
 * fade in/out opacity by scrolling up/down over a focussed image
 * press `r` to reset image size
 * press `q` to close

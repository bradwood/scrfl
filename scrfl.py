"""Scrfl - A screenflow clone"""

from PyQt5.QtCore import Qt, QPoint, QRect, QSize, QMargins
from PyQt5.QtGui import QImage, QPixmap, QPainter, QCursor, QColor, QPen
from PyQt5.QtWidgets import QLabel, QSizePolicy, QMessageBox, QMainWindow

from ewmh import EWMH


class QImageViewer(QMainWindow):
    def __init__(self, fileName):
        super().__init__()
        self.resizeZoneSelected = False
        self.adjXfac = self.adjYfac = 0
        self.transXfac = self.transYfac = 0
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setAttribute(Qt.WA_NoSystemBackground, True)

        # panel flags
        flags = 0
        flags = Qt.FramelessWindowHint  # no border and no title bar
        flags |= Qt.WindowStaysOnTopHint   # always on bottom level
        flags |= Qt.Tool  # no icon on taskbar
        self.setWindowFlags(flags)

        self.imageLabel = QLabel()
        self.imageLabel.setSizePolicy(QSizePolicy.Ignored, QSizePolicy.Ignored)
        self.imageLabel.setScaledContents(True)

        if fileName:
            image = QImage(fileName)
            if image.isNull():
                QMessageBox.information(self, "Image Viewer", "Cannot load %s." % fileName)
                exit(1)

        self.fileName = fileName
        self.opacity = 255

        painter = QPainter()
        painter.begin(image)
        pen = QPen(QColor(0, 0, 0))
        pen.setWidth(1)
        painter.setPen(pen)
        painter.drawRect(image.rect() - QMargins(1, 1, 1, 1))
        painter.end()

        self.image = image

        pm = QPixmap.fromImage(image)
        self.imageLabel.setPixmap(pm)
        self.orig_height = pm.height()
        self.orig_width = pm.width()
        self.setCentralWidget(self.imageLabel)
        self.resize(self.orig_width, self.orig_height)

        self.setGeometry(QRect(QCursor.pos() - QPoint(self.width(), self.height()),
                               QSize(self.width(), self.height())))

        self.show()

        # show the panel in all workspaces
        # (taken from ewmh documentation)
        ewmh = EWMH()
        all_wins = ewmh.getClientList()
        wins = filter(lambda w: w.get_wm_class()[1] == 'scrfl.py', all_wins)
        for w in wins:
            ewmh.setWmDesktop(w, 0xffffffff)
        ewmh.display.flush()

    def wheelEvent(self, event):
        if event.angleDelta().y() > 0:
            # decrease opacity
            if self.opacity > 40:
                self.opacity -= 5
        elif event.angleDelta().y() < 0:
            # increase opacity
            if self.opacity < 255:
                self.opacity += 5

        self.image = QImage(self.fileName)
        painter = QPainter()
        painter.begin(self.image)
        pen = QPen(QColor(0, 0, 0))
        pen.setWidth(1)
        painter.setPen(pen)
        painter.drawRect(self.image.rect() - QMargins(1, 1, 1, 1))
        painter.setCompositionMode(QPainter.CompositionMode_DestinationIn)
        painter.fillRect(self.image.rect(), QColor(0, 0, 0, self.opacity))
        painter.end()
        pm = QPixmap.fromImage(self.image)
        self.imageLabel.setPixmap(pm)
        event.accept()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Q:
            sys.exit(0)
        elif event.key() == Qt.Key_R:
            self.resize(self.orig_height, self.orig_width)

    def mouseReleaseEvent(self, event):
        self.setCursor(Qt.ArrowCursor)
        self.resizeZoneSelected = False
        self.adjXfac = self.adjYfac = 0
        self.transXfac = self.transYfac = 0

    #  See
    # https://stackoverflow.com/questions/37047236/qt-resizable-and-movable-main-window-without-title-bar

    def mousePressEvent(self, event):
        if event.buttons() & Qt.LeftButton:
            self.mpos = event.globalPos() - self.frameGeometry().topLeft()
            self.global_mpos = event.globalPos()
            self.swidth = self.width()
            self.sheight = self.height()
            event.accept()

    def mouseMoveEvent(self, event):
        self.rs_mpos = event.globalPos() - self.frameGeometry().topLeft()
        rs_size = 50  # size of side of square resize "zone"

        # check if the mouse is in one of the 4 corner resize zones
        if (abs(self.rs_mpos.x()) < rs_size and abs(self.rs_mpos.y()) < rs_size or
           abs(self.rs_mpos.x()) > self.width() - rs_size and abs(self.rs_mpos.y()) < rs_size or
           abs(self.rs_mpos.x()) < rs_size and abs(self.rs_mpos.y()) > self.height() - rs_size or
           abs(self.rs_mpos.x()) > self.width() - rs_size and abs(self.rs_mpos.y()) > self.height() - rs_size) and \
           not self.resizeZoneSelected:

            self.resizeZoneSelected = True

            # Upper left corner
            if abs(self.rs_mpos.x()) < rs_size and abs(self.rs_mpos.y()) < rs_size:
                self.setCursor(Qt.SizeFDiagCursor)
                self.adjXfac = 1
                self.adjYfac = 1

                self.transXfac = 0
                self.transYfac = 0

            # Upper right corner
            elif abs(self.rs_mpos.x()) > self.width() - rs_size and abs(self.rs_mpos.y()) < rs_size:
                self.setCursor(Qt.SizeBDiagCursor)
                self.adjXfac = -1
                self.adjYfac = 1

                self.transXfac = 1
                self.transYfac = 0

            # Lower left corner
            elif abs(self.rs_mpos.x()) < rs_size and abs(self.rs_mpos.y()) > self.height() - rs_size:
                self.setCursor(Qt.SizeBDiagCursor)
                self.adjXfac = 1
                self.adjYfac = -1

                self.transXfac = 0
                self.transYfac = 1

            # Lower right corner
            elif abs(self.rs_mpos.x()) > self.width() - rs_size and abs(self.rs_mpos.y()) > self.height() - rs_size:
                self.setCursor(Qt.SizeFDiagCursor)
                self.adjXfac = -1
                self.adjYfac = -1

                self.transXfac = 1
                self.transYfac = 1

        if event.buttons() & Qt.LeftButton and self.resizeZoneSelected:
            too_narrow = too_short = False
            self.adjXdiff = self.adjXfac * (event.globalPos().x() - self.global_mpos.x())
            self.adjYdiff = self.adjYfac * (event.globalPos().y() - self.global_mpos.y())

            w = self.swidth - self.adjXdiff
            h = self.sheight - self.adjYdiff

            if w < rs_size * 2.5:
                too_narrow = True
                w = rs_size * 2.5

            if h < rs_size * 2.5:
                too_short = True
                h = rs_size * 2.5

            movepoint = QPoint(self.mpos.x() - self.transXfac * self.adjXdiff,
                               self.mpos.y() - self.transYfac * self.adjYdiff)
            move_dest = event.globalPos() - movepoint

            if too_short:  # height
                move_dest = QPoint(move_dest.x(), self.frameGeometry().topLeft().y())

            if too_narrow:  # width
                move_dest = QPoint(self.frameGeometry().topLeft().x(), move_dest.y())

            self.resize(w, h)
            self.move(move_dest)
            event.accept()

        else:  # we are moving the window, not resizing it
            self.setCursor(Qt.ClosedHandCursor)
            if event.buttons() & Qt.LeftButton:
                self.move(event.globalPos() - self.mpos)
                event.accept()


if __name__ == '__main__':
    import sys
    from PyQt5.QtWidgets import QApplication
    app = QApplication(sys.argv)
    imageViewer = QImageViewer(sys.argv[1])
    sys.exit(app.exec_())
